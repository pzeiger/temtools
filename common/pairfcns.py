
def xy2ind(x, y, sizex):
    """ Map 2d-array indices x, y uniquely to 1-d array index.
    """
    return int(x + y * sizex)



def cantor(x, y):
    """ Cantor pairing function
    https://en.wikipedia.org/wiki/Pairing_function#Cantor_pairing_function
    """
    return int(((x+y) * (x+y+1) + y) // 2)

