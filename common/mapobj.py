import numpy as np
import matplotlib
import matplotlib.pyplot as plt
import matplotlib.colors as colors



# ---------------------------------
# Functions
# ---------------------------------

    
    

# ---------------------------------
# Classes
# ---------------------------------

class Map():
    """ Class to process spectra.
        
        Inputs:
        spec            spectrum to be added
        
        Attributes:
        self.spec       np.ndarray containing the spectrum (or just the
                        y-values if self.xspec is set
        self.ix         
    """
    
    # ---------------------------------
    # "magic methods"
    # ---------------------------------
    def __init__(self, sizex, sizey, minmaxx, minmaxy, values=None):
        # first index of self.pixels corresponds to y-position and the second
        # index to the x-position due to the convention of indexing a matrix
        # idea is here that the matrix represents the image without
        # transformations
        self.pixels = np.empty([sizey, sizex])
        self.sizex = sizex
        self.sizey = sizey
        minmaxx = np.array(minmaxx)
        minmaxy = np.array(minmaxy)
        self.minmax = np.empty([2,2])
        self.minmax[0,:] = minmaxx
        self.minmax[1,:] = minmaxy
        if values is not None:
            assert len(values) == self.pixels.size
            for val in values:
                self.set_pixel(val[0], val[1], val[2])
    
    
    def __repr__(self):
        return '{} {}'.format(self.__class__.__name__, self.pixels)
    
    
    # ---------------------------------
    # Class methods
    # ---------------------------------
    def xy2pixind(self, xval, yval):
        """ Convert x- and yval to pixelindex
        """
        xval = float(xval)
        yval = float(yval)
        # necessary for images where angles are not integers
        if self.minmax[0,0] == self.minmax[0,1]:
            indx = 0
        else:
            dx = (self.sizex-1) / (self.minmax[0,1]-self.minmax[0,0])
            indx = (xval-self.minmax[0,0]) * dx
        
        if self.minmax[1,0] == self.minmax[1,1]:
            indy = 0
        else:
            dy = (self.sizey-1) / (self.minmax[1,1]-self.minmax[1,0]) 
            indy = (self.minmax[1,1]-yval) * dy
        return int(indx), int(indy)
    
    
    def pixind2xy(self, indx, indy):
        """ 
        """
        indx = int(indx)
        indy = int(indy)
        # necessary for images where angles are not integers
        if self.minmax[0,0] == self.minmax[0,1]:
            xval = self.minmax[0,0]
        else:
            dx = (self.sizex-1) / (self.minmax[0,1]-self.minmax[0,0])
            xval = self.minmax[0,0] + indx*dx
        
        if self.minmax[1,0] == self.minmax[1,1]:
            yval = self.minmax[0,0] 
        else:
            dy = (self.sizey-1) / (self.minmax[1,1]-self.minmax[1,0]) 
            yval = self.minmax[1,1] + indy*dy
        
        return xval, yval
    
    
    def set_pixel(self, xval, yval, pixelval):
        """ 
        """
        indx, indy = self.xy2pixind(xval, yval)
#        print('set_pixel():', xval, yval, '->', indy, indx)
#        print(pixelval)
        # mind the convention of indexing a matrix
        self.pixels[indy, indx] = pixelval
        return None
    
    
    def plot(self, fig=None, ax=None, title=None, xlabel=None, ylabel=None,
            clim='auto', xticks=False, yticks=False, log=False, showcbar=True,
            y2label=None, reversecmap=False):
        """ Plot efdiff pattern.
        energy      energy loss at which efdif pattern shall be plotted
        ispec       number of the spectrum
        """
        if fig is None and ax is None:
            fig, ax = plt.subplots()
        if clim == 'auto':
            ninfpix = np.logical_not(np.isinf(self.pixels))
            nnaninfpix = np.logical_not(np.isnan(ninfpix))
            vmin = self.pixels[nnaninfpix].min()
            vmax = self.pixels[nnaninfpix].max()
        else:
            vmin = float(clim[0])
            vmax = float(clim[1])
#        extent = list(self.minmax.reshape([-1]))
#        if reversecmap:
#            cmap = matplotlib.cm.viridis_r
#            cmap.set_under(color='w')
#            cmap.set_over(color='k')
#            cmap.set_bad(color='r')
#        else:
#            cmap = matplotlib.cm.viridis
#            cmap.set_under(color='k')
#            cmap.set_over(color='w')
#            cmap.set_bad(color='r')
        if reversecmap:
            cmap = matplotlib.cm.coolwarm_r
            cmap.set_under(color='w')
            cmap.set_over(color='w')
            cmap.set_bad(color='w')
        else:
            cmap = matplotlib.cm.coolwarm
            cmap.set_under(color='w')
            cmap.set_over(color='w')
            cmap.set_bad(color='w')
        if log:
            if (self.pixels >= 0).all():
                imshowobj = ax.imshow(
                    self.pixels, interpolation='none', 
                    norm=colors.LogNorm(vmin=vmin, vmax=vmax), cmap=cmap,
#                    extent=extent,
                )
            else:
                norm = colors.SymLogNorm(
                    linthresh=0.03, linscale=1.0, vmin=vmin, vmax=vmax
                )
                imshowobj = ax.imshow(
                    self.pixels, interpolation='nearest', cmap=cmap,
                    norm=norm,
#                    extent=extent,
                )
        else:
            imshowobj = ax.imshow(
                self.pixels, interpolation='nearest', cmap=cmap,
                norm=colors.Normalize(vmin=vmin, vmax=vmax),
#                extent=extent,
            )
        if title is not None:
            ax.set_title(title)
        if xlabel is not None:
            ax.set_xlabel(xlabel)
        if ylabel is not None:
            ax.set_ylabel(ylabel)
#        if y2label is not None:
#            ax.text(1.05,  ha='center', va='center', transform=ax.transAxes)
        ax.spines['bottom'].set_color('#808080')
        ax.spines['top'].set_color('#808080')
        ax.spines['right'].set_color('#808080')
        ax.spines['left'].set_color('#808080')
#        ax.set_axis_off()

        if xticks:
            if self.sizex <= 9:
                div = 2
            else:
                div = 5
            mod = (self.sizex-1) % div
            ax.set_xticks(np.arange(0, self.sizex, div))
            tspacing = (self.minmax[0,1] - self.minmax[0,0]) / (self.sizex-1)
            upperlim = self.minmax[0,1] - (mod-1) * tspacing
            spacing = div * tspacing
            tickxlabel = np.arange(self.minmax[1,0], upperlim, spacing)
            tickxlabel[-1] = 0.0
            ax.set_xticklabels(
                np.arange(self.minmax[0,0], upperlim, spacing),
            )
        else:
            ax.tick_params(
                axis='x',
                which='both',
                bottom=False,
                top=False,
                labelbottom=False,
            )
        if yticks:
            if self.sizey <= 9:
                div = 2
            else:
                div = 5
            mod = (self.sizey-1) % div
            ax.set_yticks(np.arange(self.sizey-1, -mod-1, -div))
            tspacing = (self.minmax[1,1] - self.minmax[1,0]) / (self.sizey - 1)
            upperlim = self.minmax[1,1] - (mod-1) * tspacing
            spacing = div * tspacing
            ax.set_yticklabels(
                np.arange(self.minmax[1,0], upperlim, spacing),
            )
        else:
            ax.tick_params(
                axis='y',
                which='both',
                left=False,
                right=False,
                labelleft=False,
            )
        
        return fig, ax, imshowobj


