import pickle

# This module is inspired by the following thread on Stackoverflow:
# https://stackoverflow.com/questions/4529815/saving-an-object-data-persistence


def get_pickled(filename):
    """ Return an iterable of saved (pickled) data.
    """
    with open(filename, 'rb') as f:
        while True:
            try:
                yield pickle.load(f)
            except EOFError:
                break


def save_pickled(item, filename):
    """ Save (pickle) item(s) to file filename.
    """
    fh = open(filename,'wb')
    if isinstance(item, str):
        pickle.dump(item, fh, -1)
    else:
        try:
            for it in item:
                pickle.dump(it, fh, -1)
        except TypeError:
            print(item)
            pickle.dump(item, fh, -1)
    


