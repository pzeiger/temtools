import numpy as np
import itertools
from itertools import accumulate, repeat, chain


def cartesian_product_pp(arrays, out=None):
    """ Stolen from:
        https://stackoverflow.com/a/49445693/577088
    """
    print(arrays)
    la = len(arrays)
    print(la)
    L = *map(len, arrays), la
    dtype = np.result_type(*arrays)
    arr = np.empty(L, dtype=dtype)
    arrs = *accumulate(
        chain((arr,), repeat(0, la-1)), np.ndarray.__getitem__),
    idx = slice(None), *itertools.repeat(None, la-1)
    for i in range(la-1, 0, -1):
        arrs[i][..., i] = arrays[i][idx[:la-i]]
        arrs[i-1][1:] = arrs[i]
    arr[..., 0] =  arrays[0][idx]
    return arr.reshape(-1, la)


