import numpy as np
import matplotlib
import matplotlib.pyplot as plt
import matplotlib.colors as colors
from common import diffpatt as dp
from common import specobj
from common import pairfcns
from common import iterabletools


class EfdifPattPixel(specobj.Spec):
    """ Class that describes one pixel in an EFDIF pattern.
        ATTRIBUTES:
        self.intscs         list of integrated scattering cross section (first
                            column) and their respective integration ranges)
        self.spec           np.ndarray containing the spectrum (or just the
                            y-values if self.ix is set)
        self.ix             integer specifying the index of an external numpy array
                            storing the x-values of the spectrum
    """
    def __init__(self, spec=[], ix=None, intscs=None, intrange=None, xvals=[]):
        super().__init__(spec, ix=ix)
#        self.set_spec(spec, ix)
        self.intscs = []
        self.spec_pp = np.array([])
        if intscs != None:
            self.append_intscs(intscs, intrange)
    
    
    def __repr__(self):
        return '[{} {} {}]'.format(self.__class__.__name__,
            self.intscs, self.spec,
            )
    
    
    def cumulative_integral(self, xvals=None):
        """ Integrate scs.
            Appends the result automatically to self.intscs list.
            
            ARGUMENTS:
            intrange        integration range
            (xvals)         x-vals to use for integration in case self.spec
                            contains just y-vals
            
            RETURNS:
            intscs          value of the integral
        """
        assert self.spec.size != 0
        # Get values
        if hasattr(self, 'ix'):
            xvals = np.array(xvals)
            yvals = self.spec[0,:]
        else:
            xvals = self.spec[0,:]
            yvals = self.spec[1,:]
        # Get integration range
        out = np.empty([2, xvals.size])
        out[0,:] = xvals
        out[1,0] = .0 
        for i in range(1,xvals.size):
            dx = xvals[1:i+1] - xvals[:i]
            mf = yvals[1:i+1] + yvals[:i]
            out[1,i] = np.sum(dx * mf /2)
        return EfdifPattPixel(out)
    
    
    def integrate_scs(self, intrange, xvals=None):
        """ Integrate scs.
            Appends the result automatically to self.intscs list.
            
            ARGUMENTS:
            intrange        integration range
            (xvals)         x-vals to use for integration in case self.spec
                            contains just y-vals
            
            RETURNS:
            intscs          value of the integral
        """
        assert self.spec.size != 0
        # Get values
        if hasattr(self, 'ix'):
            xvals = np.array(xvals)
            yvals = self.spec[0,:]
        else:
            xvals = self.spec[0,:]
            yvals = self.spec[1,:]
        # Get integration range
        if iterabletools.check(intrange):
            if intrange == 'all':
                intrange = [xvals[0], xvals[-1]]
            elif isinstance(intrange, np.ndarray):
                assert intrange.size == 2
            else:
                assert len(intrange) == 2
        else:
            raise NotImplementedError('intrange not supported')
#        print(intrange)
#        print(xvals)
#        print(self.spec)
        
        # Perform simple integration ("trapezoidal rule")
        intrange = np.array(intrange)
        xstart = np.where(xvals >= intrange[0])[0][0]
        xend = np.where(xvals <= intrange[1])[0][-1]
        dx = xvals[xstart+1:xend+1] - xvals[xstart:xend]
        mf = yvals[xstart+1:xend+1] + yvals[xstart:xend]
        intscs = np.sum(dx * mf /2)
        self.append_intscs(intscs, intrange)
        return intscs
    
    
    def append_intscs(self, intscs, intrange=[]):
        """ Append value of integrated scs and corresponding integration
            range to self.intscs.

            ARGUMENTS:
            intscs          value of integral
            intrange        integration range
            
            RETURNS:
            None
        """
        self.intscs.append([float(intscs), intrange])
    
    
    def plot(self, xvals=None, xshift=0., figout=None, xlim='auto',
             ylim='auto', label=None, log=False, fig=None, ax=None,
             quiet=True):
        """ Plot spectrum associated with the pixel.
            ARGUMENTSs):
            (xvals)         xvalues to be used. If not given, it is
                            assumed that self.spec contains two rows,
                            one for xvals and one for the yvals
        """
        new = False
        if fig is None and ax is None:
            fig, ax = plt.subplots()
        
        tspec = self.xshift_spec(xshift, xvals=xvals)
        xvals = tspec[0,:]
        yvals = tspec[1,:]
        
        if label != None:
            ax.plot(xvals, yvals, label=label)
        else:
            ax.plot(xvals, yvals)
        if not quiet:
            plt.show(fig)
        
        if figout is not None:
            fig2, ax2 = plt.subplots()
            ax2.set_xlabel('energy')
            ax2.set_ylabel('ddscs')
            if log:
                ax2.set_yscale('log')
            if xlim != 'auto':
                ax2.set_xlim([float(xlim[0]), float(xlim[1])])
            if ylim != 'auto':
                ax2.set_ylim([float(ylim[0]), float(ylim[1])])
            if label != None:
                ax2.plot(xvals, yvals, label=label)
            else:
                ax2.plot(xvals, yvals)
            
            fig2.savefig(figout)
            plt.close(fig2)
        
        return fig, ax
    
    
    def mlmsratio(self, qavgrange, xvals=None):
        """ Compute ml/ms ratio according to the formula:
            
            ml/ms = 2*q / (9*p - 6*q)
        """
        if hasattr(self, 'ix'):
            yvals = self.spec[0]
        else:
            xvals = self.spec[0]
            yvals = self.spec[1]
        # Compute p
        if np.absolute(yvals).max() == yvals.max():
            p = yvals.max()
        else:
            p = yvals.min()
        idmin = np.where(xvals >= qavgrange[0])[0][0]
        idmax = np.where(xvals <= qavgrange[1])[0][-1]
        tmp = yvals[idmin:idmax+1]
        q = np.average(tmp)
        print('p=%.3f, q=%.3f' % (p, q))
        out = float(2.*q / (9.*p - 6.*q))
        print('ml/ms=%.3f' % (out))
        return out
    
    
    def postedge_normalisation(self, intrange, xvals=None, inplace=False):
        """ 
        """
        if intrange[0] == intrange[1]:
            print('intrange[0] == intrange[1], Setting peint=1')
            peint = 1
            if inplace:
                out = None
            else:
                if xvals is not None:
                    out = np.empty([self.spec.shape[0]+1,self.spec.shape[1]])
                    out[0,:] = xvals
                else:
                    out = self.spec
                    out = EfdifPattPixel(out)
        else:
            peint = self.integrate_scs(intrange, xvals) / (intrange[1] - intrange[0])
            if inplace:
                if xvals is not None:
                    self.spec = self.spec / peint
                else:
                    self.spec[1:,:] = self.spec[1:,:] / peint
                out = None
            else:
                if xvals is not None:
                    out = np.empty([self.spec.shape[0]+1,self.spec.shape[1]])
                    out[0,:] = xvals
                else:
                    out = self.spec
                out[1:,:] = self.spec[1:,:] / peint
                out = EfdifPattPixel(out)
        return out, peint



class EfdifPatt():
    """ Class for processing EFDIF patterns
    """
    
    def __init__(self, sizex, sizey, mmangx, mmangy, energies=[]):
        self.pixels = [EfdifPattPixel() for i in range(int(sizex * sizey))]
        self.sizex = sizex
        self.sizey = sizey
        self.mmangx = mmangx
        self.mmangy = mmangy
        self.energies = energies
    
    
    def pixind(self, x, y):
        """ Return index of self.pixels corresponding to 2d-image indices.
        """
        return pairfcns.xy2ind(x, y, self.sizex)
    
    
    def set_pixel(self, x, y, efdiffpattpix):
        """
        """
        ind = self.pixind(x,y)
        if isinstance(efdiffpattpix, EfdifPattPixel):
            self.pixels[ind] = efdiffpattpix
        else:
            raise ValueError('efdiffpattpix is not a valid efdiffpatt pixel')
    
    
    def inenergies(self, array):
        """
        """
        array = np.array(array)
        out = False
        ind = -1
        for energ in self.energies:
            if np.array_equiv(energ, array):
                out = True
                ind = self.energies.index(energ)
        return out, ind


    def append_energies(self, energ):
        """
        """
        energ = np.array(energ)
        self.energies.append(specobj.swapaxes_spec(energ))


    def modify_pixel(self, x, y, spec=[], intscs=None, intrange=None):
        """ Modify pixel at pos x, y.
        Arguments:
        spec            spectrum to process, must contain at least two
        rows/columns with data. The first one is treated to contain the
        x-values
        intscs          integrated scs
        intrange        scs integration range
        """
        ind = self.pixind(x,y)
        if spec != []:
            # Align spectrum
            tmp = np.array(spec)
            tmp = specobj.swapaxes_spec(spec)
            assert tmp.shape[0] == 2, 'read data is not a spectrum'
            
            # Separate energies and values
            xval = tmp[0,:]
            yval = tmp[1:,:]
            
            ine, ie = self.inenergies(xval)
            if not ine:
                self.append_energies(xval)
            self.pixels[ind].set_spec(yval, ie)
        else:
            print('Spectrum empty. Nothing to be done.')
        
        # Read integrated spectrum
        if intscs != None:
            self.pixels[ind].append_intscs(intscs, intrange)
        
        if intscs == None and intrange != None:
            energ = self.energies[self.pixels[ind].ix]
            intscs = self.pixels[ind].integrate_scs(intrange, xvals=energ)
            self.pixels[ind].set_intscs(intscs, intrange)
    
    
    def append2pixel(self, x, y, intscs=[], spec=[]):
        """
        """
        ind = self.pixind(x, y)
        if isinstance(self.pixels[ind], EfdifPattPixel):
            self.pixels[ind].append_intscs_spec(intscs, spec)
        else:
            self.pixels[ind] = EfdifPattPixel(intscs, spec)
    
    
    def plot(self, energy, intrange=[], figout='', clim='auto', log=False):
        """ Plot efdiff pattern.
        energy      energy loss at which efdif pattern shall be plotted
        ispec       number of the spectrum
        """
        grid = self.asnumpymat(energy, intrange)
#        print(grid[0,0],grid[0,-1],grid[-1,0],grid[-1,-1]) 
#        X, Y = np.mgrid[0:grid.shape[0], 0:grid.shape[1]]
#        print(X,Y)
#        print(grid)
        if clim == 'auto':
            vmin = grid.min()
            vmax = grid.max()
        else:
            vmin = float(clim[0])
            vmax = float(clim[1])
        fig, ax = plt.subplots()
        
        spx = np.abs(self.mmangx[1]-self.mmangx[0]) / (self.sizex-1)
        spy = np.abs(self.mmangy[1]-self.mmangy[0]) / (self.sizey-1)
        
        extent = [self.mmangy[0] - spx/2, self.mmangy[1] + spx/2,
                self.mmangy[0] - spy/2, self.mmangy[1] + spy/2]
        
        cmap = matplotlib.cm.coolwarm
        cmap.set_bad(color='w')
        
        if log:
            if (grid >= 0).all():
                imshowobj = ax.imshow(
                    grid, interpolation='none', 
                    norm=colors.LogNorm(vmin=vmin, vmax=vmax), cmap='coolwarm',
                    origin='lower', extent=extent,
                )
            else:
                imshowobj = ax.imshow(
                    grid, interpolation='none', cmap='coolwarm',
                    norm=colors.SymLogNorm(linthresh=0.03, linscale=1.0, vmin=vmin, vmax=vmax),
                    origin='lower', extent=extent,
                )
        else:
            if (grid >= 0).all():
                imshowobj = ax.imshow(
                    grid, interpolation='none', cmap='coolwarm',
                    norm=colors.Normalize(vmin=vmin, vmax=vmax),
                    origin='lower', extent=extent,
                )
            else:
                imshowobj = ax.imshow(
                    grid, interpolation='none', cmap='coolwarm',
                    norm=colors.Normalize(vmin=vmin, vmax=vmax),
                    origin='lower', extent=extent,
                )
        cbar = fig.colorbar(imshowobj, )
        ax.spines['bottom'].set_color('#808080')
        ax.spines['top'].set_color('#808080')
        ax.spines['right'].set_color('#808080')
        ax.spines['left'].set_color('#808080')
        ax.set_xlabel(r'$\theta_x$ / mrad')
        ax.set_ylabel(r'$\theta_y$ / mrad')
        if figout != '':
            if figout[-4:] == '.eps':
                fig.set_dpi(600)
            fig.savefig(figout)
        return fig, ax, imshowobj
    
    
    def plot_spectrum(
            self, x, y, xshift, figout='', xlim='auto', ylim='auto',
            label=None, log=False, fig='', ax=''):
        """
        """
        ind = self.pixind(x, y)
        # handle eventuality that self.energies is empty
        if hasattr(self.pixels[ind], 'ix'):
            ie = self.pixels[ind].ix
            fig, ax = self.pixels[ind].plot(
                self.energies[ie], xshift, figout=figout,
                xlim=xlim, ylim=ylim, label=label, log=log,
                fig=fig, ax=ax,
            )
        else:
            fig, ax = self.pixels[ind].plot(
                [], xshift, figout=figout, xlim=xlim, ylim=ylim,
                label=label, log=log, fig=fig, ax=ax,
            )
        return fig, ax
    
    
    def asnumpymat(self, energy, intrange=None):
        """
        """
#        print(energy,intrange,self.sizex,self.sizey)
        grid = np.empty((self.sizex * self.sizey))
        grid.fill(np.nan)
        assert grid.size == len(self.pixels), 'fewer pixel than expected'
        # plot over energy integrated ddscs
        if energy == 'int':
            if intrange is None:
                # Needs love
                for i, pix in zip(range(grid.size), self.pixels):
#                    print(i, pix.intscs)
                    grid[i] = pix.intscs
            else:
                for i,pix in zip(range(grid.size), self.pixels):
                    if hasattr(pix, 'ix'):
                        xvals = self.energies[pix.ix]
                    else:
                        xvals = None
                    
                    grid[i] = pix.integrate_scs(intrange, xvals)
        else:
            for i,pix in zip(range(grid.size), self.pixels):
                grid[i] = pix.x2yval(energy)
        return grid.reshape((self.sizex, self.sizey))
    
    
    def get_spectrum(self, x, y):
        """ Return a 2d numpy array containing the spectrum including
            x-values.
            
            ARGUMENTS:
            x               x-position of spectrum
            y               y-position of spectrum
            
            RETURNS:
            ospec           spectrum object
            xval            corresponding x-values or None if they are
                            included in ospec
        """
        ospec = self.pixels[self.pixind(x,y)]
        if hasattr(ospec, 'ix'):
            xval = self.energies[ospec.ix]
        else:
            xval = None
        return ospec, xval



class EfdifPattCuboid():
    """Structure to store a data cuboid of diffraction patterns at varying
    thicknesses including 
    """
    
    def __init__(self, sizex, sizey, nt, mmangx, mmangy):
        self.slices = [EfdifPatt(sizex, sizey, mmangx, mmangy) for i in range(nt)]
    
    def modify_pixel(
            self, x, y, it, spec=None, intscs=None, intrange=None,
            ):
        """
        """
        self.slices[it].modify_pixel(x, y, spec, intscs, intrange)
    
    
    def set_pixel(self, x, y, it, spec=None, intscs=None, intrange=None):
        """
        """
        pix = EfdiffPattPix(spec, intscs, intrange)
        self.slices[it].pixels.set_pixel(x, y, pix)
    
    
    def reduce_cuboid(self, ithickness):
        """ UNDER CONSTRUCTION
        """
#        sizex = []
#        sizey = []
#        mmangx = []
#        mmangy = []
#        nt = 0
#        out = []
#        for it in ithickness:
#            sizex.append(self.slices[it].sizex)
#            sizey.append(self.slices[it].sizey)
#            mmangx.append(self.slices[it].mmangx)
#            mmangy.append(self.slices[it].mmangy)
#            nt += 1
#        sizex = max(sizex)
#        sizey = max(sizey)
#        mmangx = np.array([np.array(mmangx[0]).min()
#            np.array(mmangx[1]).max()])
#        mmangy = np.array([np.array(mmangx[0]).min()
#            np.array(mmangx[1]).max()])
#        out = EfdifPattCuboid(sizex, sizey, nt, mmangx, mmangy)
#
#        return 
            



