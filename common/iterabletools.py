def check(some_object):
    """ Check whether some_object is iterable.
    Returns:
    True or False
    """
    try:
        some_object_iterator = iter(some_object)
        out = True
    except TypeError as te:
#        print(some_object, 'is not iterable')
        out = False
    return out


def obj2list(some_object):
    """ Quick'n dirty object to list object converter.
    """
    if check(some_object):
        some_object = list(some_object)
    else:
        some_object = [some_object]
    return some_object

