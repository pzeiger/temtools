import numpy as np
import matplotlib.pyplot as plt



# ---------------------------------
# Functions
# ---------------------------------
def swapaxes_spec(array):
    """ Align axes of array such that the shortest dimension is 0.
    """
    # Swap axes if value pairs are along tmp[:,0]
    if array.ndim == 2:
        if array.shape[1] < array.shape[0]:
            array = array.swapaxes(0,1)
    return array


def is_spec(array):
    """ Check whether array is a spectrum or not.
        
        ARGUMENTS:
        array           array to check

        RETURNS:
        boolean
    """
    isnp = isinstance(array, np.ndarray)
    isle2d = array.ndim <= 2
    hasmax2rows = np.min(array.shape) <= 2
    return (isnp and isle2d and hasmax2rows)


def is_2dspec(array):
    """ Check whether array is a 2d spectrum or not, i.e. if it contains
        x-values or not.
        
        ARGUMENTS:
        array           array to check

        RETURNS:
        boolean
    """
    isnp = isinstance(array, np.ndarray)
    isle2d = array.ndim == 2
    hasmax2rows = np.min(array.shape) <= 2
    return (isnp and isle2d and hasmax2rows)


def check_overlap(spec1, spec2):
    """ Check wheter the 0 components of numpy ndarrays spec1 and spec2
        overlap.

        ARGUMENTS:
        spec1, spec2    spectra to be checked

        RETURNS:
        boolean
    """
    spec10 = spec1[0,:]
    spec20 = spec2[0,:]
    mask10 = (spec20.min() <= spec10) * (spec10 <= spec20.max())
    mask20 = (spec10.min() <= spec20) * (spec20 <= spec10.max())
    mask1 = np.squeeze((mask10, mask10))
    mask2 = np.squeeze((mask20, mask20))
    
    if (spec10[np.invert(mask10)] < spec20.min()).all():
        order = [spec1, spec2]
        overlap = [mask1, mask2]
    else:
        order = [spec2, spec1]
        overlap = [mask2, mask1]

    # return no overlap if grid different
    if (spec10[mask10] != spec20[mask20]).any():
        overlap = None
    return order, overlap


def xshift_spec(spec, xshift):
    """ Shift x-values of a spectrum.
    """
    spec[0,:] += xshift
    return spec
    
    
    

# ---------------------------------
# Classes
# ---------------------------------

class Spec():
    """ Class to process spectra.
        
        Inputs:
        spec            spectrum to be added
        
        Attributes:
        self.spec       np.ndarray containing the spectrum (or just the
                        y-values if self.xspec is set
        self.ix         
    """
    
    # ---------------------------------
    # "magic methods"
    # ---------------------------------
    def __init__(self, spec, ix=None):
        self.set_spec(spec, ix) 
    
    def __repr__(self):
        return '{} {}'.format(self.__class__.__name__, self.spec)
    
    
    # ---------------------------------
    # Class methods
    # ---------------------------------
    def broaden_lorentz(self, gam, xmin, xmax, xstep):
        """ Broaden and shift spectrum by lorentzian.
            Based on programm "lorentz" by Ján Rusz.
        """
        ene_out = np.arange(xmin, xmax + xstep, xstep)
        x1 = self.spec[0,:-1]
        x2 = self.spec[0,1:]
        y1 = self.spec[1,:-1]
        y2 = self.spec[1,1:]
        ai = (y2-y1)/(x2-x1)
        bi = y1 - ai*x1
        out = np.zeros([2,ene_out.size])
        out[0,:] = ene_out
        for x, y in np.nditer([out[0,:], out[1,:]], 
            flags = ['buffered'],
            op_flags=[['readwrite'],['readwrite']]):
            y[...] = np.sum(gam*ai/(2.*np.pi) * np.log(((x-x2)*(x-x2)+gam*gam) / \
                ((x-x1)*(x-x1)+gam*gam)) - (ai*x+bi)/np.pi * \
                (np.arctan((x-x2)/gam)-np.arctan((x-x1)/gam)))
        return out 
   

    def normalise_to_unity(self, speclist):
        """ 
        """
        maxval = self.spec[1:,:].max()
        for sp in speclist:
            tmp = sp.spec[1:,:].max()
#            print(tmp)
#            print(sp.spec[1,:])
            if tmp > maxval:
                maxval = tmp
        self.spec[1:,:] = self.spec[1:,:] / maxval
        for sp in speclist:
            sp.spec[1:,:] = sp.spec[1:,:] / maxval
        return speclist
    
    
    def normalise_to_value(self, value, inplace=False):
        """ 
        """
        if inplace:
            self.spec[1:,:] = self.spec[1:,:] / value
            out = None
        else:
            out = self.spec
            out[1:,:] = out[1:,:] / value
        return out 
    
    
    def check_overlap(self, spec2, xval=None):
        """ Check if self.spec and spec2 overlap.
            
            ARGUMENTS:
            spec2           numpy 2d-array to be checked for overlap
            
            RETURNS:
            bool            True if overlap exists
        """
        if not hasattr(self, 'ix'):
            assert xval is None
            spec1 = self.spec
        else:
            spec1 = np.empty([2, xval.size])
            spec1[0,:] = xval
            spec1[1,:] = self.spec
            spec1 = swapaxes_spec(spec1)
        order, overlap = check_overlap(spec1, spec2)
        return order, overlap
    
    
    def add_spectra(self, spec2, xval1=None):
        """ Add spectrum to the existing one.
            
            ARGUMENTS:
            spec2           2d numpy array
            xval1           x-values if self.spec is 1-d numpy array
        """
#        print('Adding spectra...')
        if spec2 != np.array([]):
            assert is_2dspec(spec2)
            spec2 = swapaxes_spec(spec2)
            
            # check for overlap and get right order of spectra to add
            order, ov = self.check_overlap(spec2, xval=xval1)
            assert ov is not None, \
                'spectra are overlapping and xvals differ in overlap'
            ov0 = order[0][ov[0]].reshape([2,-1])
            no0 = order[0][np.invert(ov[0])].reshape([2,-1])
            ov1 = order[1][ov[1]].reshape([2,-1])
            no1 = order[1][np.invert(ov[1])].reshape([2,-1])
            ov01 = np.squeeze((ov0[0,:], ov0[1,:]+ov1[1,:]))
            aspec = np.concatenate((no0, ov01, no1), axis=1)
        else:
            if hasattr(self, 'ix'):
                aspec = np.concatenate((xval1,self.spec))
                assert is_2dspec(aspec)
            else:
                aspec = self.spec
        return aspec 
    
    
    def plot(self, xvals=None, ax=None, fig=None):
        """ Plot spectrum
        """
        if hasattr(self, 'ix'):
            yvals = self.spec
        else:
            xvals = self.spec[0,:]
            yvals = self.spec[1,:]
        fig, ax = plt.subplots()
        ax.plot(xvals, yvals)
        return fig, ax
    

    def set_spec(self, spec, ix=None):
        """ Set spectrum.
        """
        if ix is not None:
            self.ix = int(ix)
        spec = np.array(spec)
        assert is_spec(spec), 'array is not a spectrum'
        self.spec = swapaxes_spec(spec)
    
    
    def remove_xvals(self, ix):
        """
        """
        assert not hasattr(self, 'ix')
        self.ix = int(ix)
        xvals = self.spec[0,:]
        self.spec = self.spec[1,:]
        return xvals
    
    
    def xshift_spec(self, xshift, xvals=None, inplace=False):
        """ Shift spectrum
        """
        if hasattr(self, 'ix'):
            assert xvals is not None
            out = np.empty([2,xvals.size])
            out[0,:] = xvals
            out[1,:] = self.spec
        else:
            out = self.spec
        out = xshift_spec(out, xshift)
        if inplace:
            self.spec = out
            del self.ix
        
        return out
    
    
    def get_spec(self, xvals=None):
        """ Return spectrum
        """
        if hasattr(self, 'ix') :
            assert xvals != None
        else:
            ospec = np.array([xval, self.spec])
            ospec = swapaxes_spec(ospec)
        return ospec
    
    
    def x2yval(self, xval):
        """
        """
        return self.spec[1,np.where(self.spec[0,:] == xval)]
    
    
    def y2xval(self, yval):
        """
        """
        return self.spec[0,np.where(self.spec[1,:] == yval)]

