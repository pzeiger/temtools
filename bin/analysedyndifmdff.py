#!/usr/bin/python3
import sys
import pathlib

# Add parent
dir_root = pathlib.Path(pathlib.Path(__file__).resolve().parents[1])

if not dir_root in sys.path:
    sys.path.insert(0, str(dir_root))

# Import and run
from dyndifmdff import plot

if __name__ == "__main__":
    analyse.main(sys.argv)

