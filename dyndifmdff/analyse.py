#!/usr/bin/python3

import sys # for exiting on errors
import argparse # easy command line args
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import IPython
from dyndifmdff import dyndifmdffobjs as dmo
from dyndifmdff import aggregate
from common import pickletools as pt



def get_data_obj(datafile):
    """ 
    """
    # make list to unify syntax
    if isinstance(datafile, str):
        args.datafile = [datafile]
    
    for filename in datafile:
        assert filename[-11:] == '.aggregated'
        try:
            for dat in pt.get_pickled(filename):
                yield dat, filename
        except:
            pass


def emcd(args):
    """ Plot diffraction patterns
    """
    # Get generator object
    gen = get_data_obj(args.datafile)
    
    # Load data and plot data
    for dat,filename in gen:
        # interpret edge setting
        if args.edge[0] == 'all':
            edge = range(len(dat.edge))
        else:
            edge = [ int(i) for i in args.edge]
        
        if args.interactive:
            IPython.embed()
        
        fileprefix = 'diffpatt_' + filename[:-11]
        dat.plot_diffpatt(
            edge, args.thickness, args.energy, fileprefix,
            args.energyrange, args.clim, args.logarithmicscale,
            args.quiet,
        )


def parse_cmd_args(argv):
    """Parse command line arguments using argparse
    """
    
    # Set up command line arg parser
    if argv is None:
        argv = sys.argv
    parser = argparse.ArgumentParser(
        prog=argv[0],
        description='Program to plot pickled dyndiff-mdff simulation data',
    )
    parser.add_argument(
        '-i', '--interactive', action='store_true',
        help='command line switch to enter interactive mode',
    )
    parser.add_argument(
        '-q', '--quiet', action='store_true',
        help='quiet mode. No plots are shown.',
    )
    parser.add_argument(
        '-v', '--version', action='version', version='%(prog)s 0.1',
    )
    
    # Add subparsers to select type of plot
    subparsers = parser.add_subparsers(
        title='Plot mode',
        help='Select wether to plot diffraction pattern or spectra',
    )

    # Parser for spectra plotting
    parser_spec = subparsers.add_parser(
        'spec', help='specify that energy spectrum is to be plotted'
    )
    parser_spec.add_argument(
        '-a', '--angle', nargs=2, default = ['6.0', '6.0'],
        help='specify scattering angles (thetax and -y) for spectrum plots',
    )
    parser_spec.add_argument(
        'datafile', nargs='+', 
        help='file to be read',
    )
    parser_spec.add_argument(
        '--edge', default=['0'], nargs='+', 
        help='Specify edge(s) for plot',
    )
    parser_spec.add_argument(
        '-i', '--interactive', action='store_true',
        help='command line switch to enter interactive mode',
    )
    parser_spec.add_argument(
        '-l', '--logarithmicscale', action='store_true',
        help='command line switch to trigger log scale in plots',
    )
    parser_spec.add_argument(
        '--thickness', nargs='+', default=['20.0'],
        help='specify thickness for plot(s)',
    )
    parser_spec.add_argument(
        '-o', '--out', default='dyndifmdff_spectrum.png', 
        help='output file', type=str,
    )
    parser_spec.add_argument(
        '-y', '--ylim', nargs=2, default='auto',
        help='limits for y-axis', type=str,
    )
    parser_spec.set_defaults(func=spectra)
    
    
    # Parser for diffraction pattern plotting
    parser_diffpatt = subparsers.add_parser(
        'diffpatt', help='specify that diffraction pattern is to be plotted'
    )
    parser_diffpatt.add_argument(
        '-c', '--clim', nargs=2, default='auto',
        help='value limits for color bar', type=str,
    )
    parser_diffpatt.add_argument(
        'datafile', nargs='+', 
        help='file to be read',
    )
    parser_diffpatt.add_argument(
        '-e', '--energy', nargs=1, default='int',
        help='specify energy of interest',
    )
    parser_diffpatt.add_argument(
        '--edge', default=['0'], nargs='+', 
        help='Specify edge(s) for plot',
    )
    parser_diffpatt.add_argument(
        '-l', '--logarithmicscale', action='store_true',
        help='command line switch to trigger log scale in plots',
    )
    parser_diffpatt.add_argument(
        '-o', '--out', default='dyndifmdff_diffpatt.png', 
        help='output file', type=str,
    )
    parser_diffpatt.add_argument(
        '--energyrange', nargs=2, default='all',
        help='specify energy integration range for diffraction pattern',
    )
    parser_diffpatt.add_argument(
        '--thickness', nargs='+', default=['20.0'],
        help='specify thickness for plot(s)',
    )
    parser_diffpatt.set_defaults(func=diffpatt)
    
    # Get and return command line options, skip program name in argv[0]
    argv=argv[1:]
    return parser.parse_args(argv)


def main(argv=None):
    """ Wrapper function representing the actual program.
    """
    
    # Parse command line input
    args = parse_cmd_args(argv)
    
    # Invoke associated functions
    args.func(args)
    


# run main if not imported
if __name__ == '__main__':
    sys.exit(main(sys.argv))

