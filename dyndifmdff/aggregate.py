#!/usr/bin/python3

import sys # for exiting on errors
import argparse # easy command line args
from dyndifmdff import dyndifmdffobjs as dmo


def parse_cmd_args(argv):
    """Parse command line arguments using argparse
    """
    
    # Set up command line arg parser
    if argv is None:
        argv = sys.argv
    parser = argparse.ArgumentParser(
        prog=argv[0], description='Program to load dyndiff-mdff simulations '
        'from different directories into a single data structure',
        )
    argv=argv[1:]
    parser.add_argument(
        'filename', nargs=1, help='filename to process in each dir', type=str,
    )
    parser.add_argument(
        'dirs', nargs='+', help='directories to process',
    )
    parser.add_argument(
        '-i', '--insel2file', default='Fe.insel2',
        help='dyndifmdff insel2 file',
    )
    parser.add_argument(
        '-o', '--out', default='dyndifmdff.aggregated', help='output file',
        type=str,
    )
    parser.add_argument(
        '-v', '--version', action='version', version='%(prog)s 0.1',
    )
    
    # Get and return command line options
    return parser.parse_args(argv)


def main(argv=None):
    """Wrapper function representing the actual program.
    """
    
    # Get and print command line options
    args = parse_cmd_args(argv)

    # Load data
    data = dmo.DyndifMdffData(2, args.dirs, args.filename[0],
            args.insel2file)
    
    # Save data in object
    data.save_obj(args.out)


# run main if not imported
if __name__ == '__main__':
    sys.exit(main(sys.argv))

