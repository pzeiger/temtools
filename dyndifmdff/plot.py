#!/usr/bin/python3

import sys # for exiting on errors
import argparse # easy command line args
import matplotlib
import numpy as np
import matplotlib.colors as colors
from os import environ
if 'DISPLAY' not in environ:
    matplotlib.use('Agg')
import matplotlib.pyplot as plt
import IPython
from dyndifmdff import dyndifmdffobjs as dmo
from dyndifmdff import aggregate
from common import pickletools as pt
from common import iterabletools
import warnings


def get_data_obj(datafile):
    """ 
    """
    # make list to unify syntax
    if isinstance(datafile, str):
        datafile = [datafile]
    for filename in datafile:
        assert filename[-11:] == '.aggregated'
        try:
            dat = next(pt.get_pickled(filename))
            yield dat, filename
#            for dat in pt.get_pickled(filename):
#                yield dat, filename
        except:
            pass


def apply_plotopt(
        fig, ax, xlim='auto', ylim='auto', xlabel='Energy loss / eV',
        ylabel='Intensity (au)', out='', log=False, quiet=False,
        suppressout=False, ncol=3, legendpos='h', fontsize='small'):
    """ 
    """
    ax.set_xlabel(xlabel)
    ax.set_ylabel(ylabel)
    if log:
        ax.set_yscale('log')
    if xlim != 'auto':
        ax.set_xlim([float(xlim[0]), float(xlim[1])])
    if ylim != 'auto':
        ax.set_ylim([float(ylim[0]), float(ylim[1])])
    
    ax.spines['bottom'].set_color('#808080')
    ax.spines['top'].set_color('#808080') 
    ax.spines['right'].set_color('#808080')
    ax.spines['left'].set_color('#808080')
    for ax in fig.axes:
        lines = ax.get_lines()
        lines[0].set_color('#176fc0')
        if len(lines) >= 2:
            tmpcolor = lines[1].get_color()
            lines[1].set_color('#ae2230')
        if len(lines) >= 4:
            lines[3].set_color(tmpcolor)
    
    with warnings.catch_warnings():
        warnings.filterwarnings(
            'ignore', message='No labelled objects found.'
        )
        print('legendpos: ', legendpos)
        if legendpos == 'h':
            ax.legend(
                fontsize='small', bbox_to_anchor=(0.0, 1.02, 1., .102),
                loc='lower left', ncol=ncol, mode="expand", borderaxespad=0.,
            )
        elif legendpos == 'ilr':
            ax.legend(
                fontsize='small', loc='lower right',
            )
        elif legendpos == 'ill':
            ax.legend(
                fontsize='small', loc='lower left',
            )
        elif legendpos == 'iur':
            ax.legend(
                fontsize='small', loc='upper right',
            )
        elif legendpos == 'iul':
            ax.legend(
                fontsize='small', loc='upper left',
            )
        elif legendpos == 'ov':
            ncol=1
            ax.legend(
                fontsize='small', bbox_to_anchor=(1.02, 0.0, .102, 1.),
                loc='upper left', ncol=ncol, mode="expand", borderaxespad=0.,
            )
        else:
            ax.legend(fontsize='small')
    
    fig.set_size_inches(7, 4.375)
    if out[-4] == '.eps':
        fig.set_dpi(600)
    else:
        fig.set_dpi(150)
    
    if not quiet:
        plt.show(fig)
    if not suppressout:
        fig.savefig(out, size=(7, 4.375), bbox_inches='tight')
    return fig, ax


def adddiffpatts(args):
    """ Add and plot diffraction patterns
    """
    print('Adding and plotting diffraction pattern...')
    
    # Get generator object
    gen = get_data_obj(args.datafile)
    
    print(args.sign)
    print(args.mirror)
    sign_gen = (x for x in args.sign)
    mirr_gen = (x for x in args.mirror)
    if args.normpbp:
        normcontrib_gen = (x for x in args.normcontrib)
        if args.addedges:
            diffpattsnormval = [[None for x in range(len(args.edge))] for y in
                range(len(args.thickness))]
        else:
            diffpattsnormval = [None for y in range(len(args.thickness))]
    
    # Quick and dirty implementation. Needs to be moved into dyndifmdffobjs
    # module
    assert len(args.datafile) > 1
    if args.addedges:
        addeddps = [None for y in range(len(args.thickness))]
    else:
        addeddps = [[None for x in range(len(args.edge))] for y in
            range(len(args.thickness))]
    
    for dat, filename in gen:
        print('Loading data from ' + filename)
        try:
            sign = next(sign_gen)
        except:
            print('no sign specified. Falling back to default "addition" (p).')
            sign = 'p'
            
        try:
            mirror = next(mirr_gen)
        except:
            print('no mirror operation specified. Falling back to default' \
                  '"no mirroring" (n).'
            )
            mirror = 'n'
        if args.normpbp:
            try:
                normcontrib = next(normcontrib_gen)
            except:
                print('no mirror operation specified. Falling back to default' \
                      '"no mirroring" (n).')
                normcontrib = 'n'
        
        mmangx = dat.mmang[0,:]
        mmangy = dat.mmang[1,:]
        
        if args.interactive:
            IPython.embed()
        
        for i, ie in zip(range(len(args.edge)),args.edge):
            for j in range(len(args.thickness)):
                it = dat.thickness2it(args.thickness[j])
                mat = dat.edge[ie].slices[it].asnumpymat(
                    'int', intrange='all'
                )
#                print(mirror,sign)
                if mirror[0] in 'hH':
                    print('flipping horizontally')
#                    print(mat)
                    mat = np.fliplr(mat)
#                    print(mat)
                elif mirror[0] in 'vV':
                    print('flipping vertically')
#                    print(mat)
                    mat = np.flipud(mat)
#                    print(mat)
                elif mirror[0] in 'xX':
                    print('flipping horizontally & vertically')
#                    print(mat)
                    mat = np.fliplr(np.flipud(mat))
#                    print(mat)
                
                if args.addedges:
                    if sign[0] in 'mM':
                        if addeddps[j] is None:
                            addeddps[j] = -mat
                        else:
                            addeddps[j] = addeddps[j] - mat
                    elif sign[0] in 'pP':
                        if addeddps[j] is None:
                            addeddps[j] = mat
                        else:
                            addeddps[j] = addeddps[j] + mat
                    else:
                        raise NotImplementedError
                    if args.normpbp:
                        if normcontrib[0] in 'yY':
                            if normcontrib[1] in 'mM':
                                if diffpattsnormval[j] is None:
                                    diffpattsnormval[j] = -mat
                                else:
                                    diffpattsnormval[j] = diffpattsnormval[j] - mat
                            elif normcontrib[1] in 'pP':
                                if diffpattsnormval[j] is None:
                                    diffpattsnormval[j] = mat
                                else:
                                    diffpattsnormval[j] = diffpattsnormval[j] + mat
                            else:
                                raise NotImplementedError
                    print(args.edge[i], args.thickness[j], mirror, sign,)
                    print(addeddps[j].min(), addeddps[j].max())
                    if args.normpbp and diffpattsnormval[j] is not None:
                        print(normcontrib,)
                        print(diffpattsnormval[j].min(), diffpattsnormval[j].max())
                else:
                    if sign[0] in 'mM':
                        if addeddps[j][i] is None:
                            addeddps[j][i] = -mat
                        else:
                            addeddps[j][i] = addeddps[j][i] - mat
                    elif sign[0] in 'pP':
                        if addeddps[j][i] is None:
                            addeddps[j][i] = mat
                        else:
                            addeddps[j][i] = addeddps[j][i] + mat
                    else:
                        raise NotImplementedError
                    if args.normpbp:
                        if normcontrib[0] in 'yY':
                            if normcontrib[1] in 'mM':
                                if diffpattsnormval[j][i] is None:
                                    diffpattsnormval[j][i] = -mat
                                else:
                                    diffpattsnormval[j][i] = diffpattsnormval[j][i] - mat
                            elif normcontrib[1] in 'pP':
                                if diffpattsnormval[j][i] is None:
                                    diffpattsnormval[j][i] = mat
                                else:
                                    diffpattsnormval[j][i] = diffpattsnormval[j][i] + mat
                            else:
                                raise NotImplementedError
                    
                    print(args.edge[i], args.thickness[j], mirror, sign,)
                    print(addeddps[j][i].min(), addeddps[j][i].max())
                    if args.normpbp and diffpattsnormval[j][i] is not None:
                        print(normcontrib,)
                        print(diffpattsnormval[j][i].min(), diffpattsnormval[j][i].max())
                

                
    
    
    for j in range(len(addeddps)):
        for i in range(len(addeddps[j])):
            if args.addedges:
                if args.normpbp:
                    if diffpattsnormval[j] is not None:
                        grid = (addeddps[j] - diffpattsnormval[j]) / \
                            np.abs(diffpattsnormval[j])
                else:
                    grid = addeddps[j]
            else:
                if args.normpbp:
                    if diffpattsnormval[j][i] is not None:
                        grid = (addeddps[j][i] - diffpattsnormval[j][i]) / \
                            np.abs(diffpattsnormval[j][i])
                else:
                    grid = addeddps[j][i]
            
            if args.clim == 'auto':
                nnaninfpix = grid[
                    np.logical_and(
                        np.logical_not(np.isnan(grid)),
                        np.logical_not(np.isinf(grid))
                    )
                ]
                vmin = nnaninfpix.min() 
                vmax = nnaninfpix.max() 
            else:
                vmin = float(args.clim[0])
                vmax = float(args.clim[1])
            fig, ax = plt.subplots()
            print(vmin)
            print(vmax)
            sizex = grid[0,:].size
            sizey = grid[:,0].size
            
            spx = np.abs(mmangx[1]-mmangx[0]) / (sizex-1)
            spy = np.abs(mmangy[1]-mmangy[0]) / (sizey-1)
            
            extent = [mmangy[0] - spx/2, mmangy[1] + spx/2,
                    mmangy[0] - spy/2, mmangy[1] + spy/2]
            
            cmap = matplotlib.cm.coolwarm
            cmap.set_bad(color='w')
            
            imshowobj = ax.imshow(
                grid, interpolation='none', cmap=cmap,
                norm=colors.Normalize(vmin=vmin, vmax=vmax),
                origin='lower', extent=extent,
            )
            
            cbar = fig.colorbar(imshowobj, )
            ax.spines['bottom'].set_color('#808080')
            ax.spines['top'].set_color('#808080')
            ax.spines['right'].set_color('#808080')
            ax.spines['left'].set_color('#808080')
            ax.set_xlabel(r'$\theta_x$ / mrad')
            ax.set_ylabel(r'$\theta_y$ / mrad')
            if args.addedges:
                figout = 'added_diffpatts_generic_%snm_addededges.eps' \
                    % (args.thickness[j])
            else:
                figout = 'added_diffpatts_generic_%snm_edge%i.eps' \
                    % (args.thickness[j],args.edge[i])

            if figout != '':
                if figout[-4:] == '.eps':
                    fig.set_dpi(600)
                fig.savefig(figout)
            
            if args.addedges:
                break


def diffpatt(args):
    """ Plot diffraction patterns
    """
    print('Plotting diffraction pattern...')
    
    # Get generator object
    gen = get_data_obj(args.datafile)
    
    # Load data and plot data
    for dat, filename in gen:
        print('Loading data from ' + filename)
        
        if args.interactive:
            IPython.embed()
        
        fileprefix = 'diffpatt_' + '_'.join(filename[:-11].split('/'))
        dat.plot_diffpatt(
            args.edge, args.thickness, args.energy, fileprefix,
            args.energyrange, args.clim, args.logscale,
            args.quiet,
        )



def rawdiffpatt(args):
    """ Plot raw dyndifmdff spectra
    """
    raise NotImplementedError


def emcd(args):
    """ Compute and plot "experimental" EMCD from spectra.
    """
    
    print('Computing and plotting EMCD signal...')
    
    # Get generator object
    gen = get_data_obj(args.datafile)
    
    # Get custom labels for data from different files
    label_gen = (x for x in args.datalabel)
    
    # Get emcdtype generator
    if len(args.emcdtype) == 1 and len(args.datafile) != 1:
        args.emcdtype = [args.emcdtype[0]]*len(args.datafile)
    emcdtype_gen = (x for x in args.emcdtype)
    if not [args.emcdtype[0]]*len(args.emcdtype) == args.emcdtype:
        label_emcdtype = True
    else:
        label_emcdtype = False
    
    # Load data and plot data
    fig, ax = plt.subplots()
    
    # Dict for normalisation
    penormint = None
    pedatagentheo = get_data_obj(args.penormdatatheo)
    pedatagenexp = get_data_obj(args.penormdataexp)
    
    for dat, filename in gen:
        print('Loading data from ' + filename)
        
        if args.interactive:
            IPython.embed()
            continue
        
        if args.compare:
            try:
                labelp = next(label_gen)
            except:
                labelp = ''
#            if labelp == '':
#                labelp = filename[0:-11] + ' '
        else:
            labelp = ''
        print(labelp, filename, args.npe)
        etype = next(emcdtype_gen)
        
        if etype[1:] == 'theo' and args.penormdatatheo is not None \
                and not args.npe:
            pedata, pedatafname = next(pedatagentheo)
            print('Postedge normalization for theoretical EMCD computed ' \
                  'from file %s' % pedatafname)
            if isinstance(pedata, dmo.DyndifMdffData):
                etype2 = etype[0] + 'exp'
                out = pedata.plot_calc_emcd(
                    args.edge, args.thickness, args.anglex, args.angley,
                    args.detectorsizex, args.detectorsizey, args.detectorshape,
                    etype2, args.pelow, args.peup, args.elow[0], args.eup[0],
                    args.estep[0], args.gamma, args.xshift, args.droppoints,
                    noemcdplot=args.emcdoff, quiet=True,
                    nocemcdplot=args.cemcdoff, oonlypenorm=True,
                )
                penormint = out[2]
        elif etype[1:] == 'exp' and args.penormdataexp is not None \
                and not args.npe:
            try: 
                pedata, pedatafname = next(pedatagenexp)
                print('Postedge normalization for experimental EMCD computed ' \
                      'from file %s' % pedatafname)
                if isinstance(pedata, dmo.DyndifMdffData):
                    out = pedata.plot_calc_emcd(
                        args.edge, args.thickness, args.anglex, args.angley,
                        args.detectorsizex, args.detectorsizey, args.detectorshape,
                        etype, args.pelow, args.peup, args.elow[0], args.eup[0],
                        args.estep[0], args.gamma, args.xshift, args.droppoints,
                        noemcdplot=args.emcdoff, quiet=True, 
                        nocemcdplot=args.cemcdoff, oonlypenorm=True,
                    )
                    penormint = out[2]
                else:
                    penormint = None
            except StopIteration:
                penormint = None
        print('penorm values:', penormint)
        out = dat.plot_calc_emcd(
            args.edge, args.thickness, args.anglex, args.angley,
            args.detectorsizex, args.detectorsizey, args.detectorshape,
            etype, args.pelow, args.peup, args.elow[0], args.eup[0],
            args.estep[0], args.gamma, args.xshift, args.droppoints,
            emcdnormval=penormint, normtodetarea=args.normtodetarea,
            fig=fig, ax=ax, labelp=labelp, quiet=args.quiet, nopenorm=args.npe,
            nocemcdplot=args.cemcdoff, noemcdplot=args.emcdoff,
            label_emcdtype=label_emcdtype,
            plotspecs=args.plotspectra,
        )
        fig = out[0]
        ax = out[1]
        if etype[1:] == 'theo':
            penormint = None
        else:
            penormint = out[2]
        print('penormint :', penormint)
        
        
        
        if not args.compare:
            fig, ax = apply_plotopt(
                fig, ax, ylim=args.ylim, out=args.out, log=args.logscale,
                quiet=args.quiet, suppressout=args.suppressout, ncol=args.ncol[0],
                legendpos=args.legendpos[0],
            )
            fig, ax = plt.subplots()
        
        # Overwrite object
        if args.readwrite:
            dat.save_obj(filename)
    
    
    if args.compare:
        fig, ax = apply_plotopt(
            fig, ax, ylim=args.ylim, out=args.out, log=args.logscale,
            quiet=args.quiet, suppressout=args.suppressout, ncol=args.ncol[0],
            legendpos=args.legendpos[0],
        )


def mlmsmap(args):
    """ Compute and plot maps of ml/ms of "experimental" EMCD from spectra.
    """
    
    print('Computing and plotting maps of ml/ms...')
    
    # Get generator object
    gen = get_data_obj(args.datafile)
    
    # Get custom labels for data from different files
    label_gen = (x for x in args.datalabel)
    
    # Get emcdtype generator
    emcdtype_gen = (x for x in args.emcdtype)
    
    # Dict for normalisation
    penormint = None
    pedatagentheo = get_data_obj(args.penormdatatheo)
    pedatagenexp = get_data_obj(args.penormdataexp)
    
    for dat, filename in gen:
        print('Loading data from ' + filename)
        
        if args.interactive:
            IPython.embed()
            continue
        
        if args.compare:
            try:
                labelp = next(label_gen)
            except:
                labelp = ''
#            if labelp == '':
#                labelp = filename[0:-11] + ' '
        else:
            labelp = ''
        etype = next(emcdtype_gen)
        
        if etype[1:] == 'theo' and args.penormdatatheo is not None \
                and not args.npe:
            try: 
                pedata, pedatafname = next(pedatagentheo)
                print('Postedge normalization for theoretical EMCD computed ' \
                      'from file %s' % pedatafname)
            except StopIteration:
                pedata = None
        
        elif etype[1:] == 'exp' and args.penormdataexp is not None \
                and not args.npe:
            try: 
                pedata, pedatafname = next(pedatagenexp)
                print('Postedge normalization for experimental EMCD computed ' \
                      'from file %s' % pedatafname)
            except StopIteration:
                pedata = None
        else:
            pedata = None
        
        for th in args.thickness:
            
            print(th, args.pelow, args.peup) 
            out = dat.calc_mlmsmap(
                args.edge, th, args.detcxminmax, args.detcyminmax,
                args.detectorsizex, args.detectorsizey, args.detectorshape[0],
                etype, args.pelow, args.peup, args.elow[0], args.eup[0],
                args.estep[0], args.gamma, args.xshift, pedata, args.droppoints,
                nopenorm=args.npe, mlmsint=args.mlmsint,
                mlmsintnpe=args.mlmsintnpe,
            )
            
            fig = dat.plot_mlmsratio(
                out, clim=args.clim, nopenorm=args.npe,
            )
            
            if etype[1:] == 'theo':
                penormint = None
            
            if not args.compare:
#                fig, ax = apply_plotopt(
#                    fig, ax, ylim=args.ylim, xlabel='Energy / eV',
#                    ylabel='DSCS', out=args.out, log=args.logscale,
#                    quiet=args.quiet, suppressout=args.suppressout,
#                )
#                fig.tight_layout()
                if not args.suppressout:
                    if args.out[-4] == '.eps':
                        fig.set_dpi(600)
                        fig.savefig(args.out, size=(6.5, 10.4), bbox_inches='tight')
                    else:
                        fig.set_dpi(150)
                        fig.savefig(args.out, size=(6.5, 10.4), bbox_inches='tight')
                if not args.quiet:
                    plt.show(fig)
#                fig, ax = plt.subplots()
            
    
#    if args.compare:
#        fig, ax = apply_plotopt(
#            fig, ax, ylim=args.ylim, xlabel='Energy / eV',
#            ylabel='Total DSCS', out=args.out, log=args.logscale, 
#            quiet=args.quiet, suppressout=args.suppressout,
#        )


def rawspec(args):
    """ Plot raw dyndifmdff spectra
    """
    print('Plotting raw DSCS...')
    
    # Get generator object
    gen = get_data_obj(args.datafile)
    
    # Get custom labels for data from different files
    label_gen = (x for x in args.datalabel)
    
    # Load data and plot data
    fig, ax = plt.subplots()
    
    # Load data and plot data
    for dat, filename in gen:
        print('Loading data from ' + filename)
        
        if args.interactive:
            IPython.embed()
        
        if args.compare:
            try:
                labelp = next(label_gen)
            except:
                labelp = ''
            if labelp == '':
                labelp = filename[0:-11] + ' '
        else:
            labelp = ''
        
        fig, ax = dat.plot_spectra(
            args.edge, args.thickness, args.anglex, args.angley,
            xshift=args.xshift, log=args.logscale,
            fig=fig, ax=ax, edgelabel=args.edgelabel,
        )
        
        if not args.compare:
            fig, ax = apply_plotopt(
                fig, ax, xlim=args.xlim, ylim=args.ylim, out=args.out, log=args.logscale,
                quiet=args.quiet, suppressout=args.suppressout, ncol=args.ncol[0],
                legendpos=args.legendpos[0],
            )
            fig, ax = plt.subplots()
    
    
    if args.compare:
        fig, ax = apply_plotopt(
            fig, ax, xlim=args.xlim, ylim=args.ylim, out=args.out, log=args.logscale,
            quiet=args.quiet, suppressout=args.suppressout, ncol=args.ncol[0],
            legendpos=args.legendpos[0],
        )



def ppspec(args):
    """ Plot postprocessed spectra.
    """
    print('Plotting postprocessed spectra...')
    print('Output file: %s' % args.out)
    
    # Get generator object
    gen = get_data_obj(args.datafile)
    
    # Get custom labels for data from different files
    label_gen = (x for x in args.datalabel)
    
    # Load data and plot data
    fig, ax = plt.subplots()
    
    for dat, filename in gen:
        print('Loading data from ' + filename)
        
        if args.interactive:
            IPython.embed()
        
        if args.compare:
            try:
                labelp = next(label_gen)
            except:
                labelp = ''
            if labelp == '':
                labelp = filename[0:-11] + ' '
        else:
            labelp = ''
        dictid1 = dat.edge_spectra_pp(
            args.edge, args.thickness, args.anglex, args.angley,
            args.gamma, args.elow[0], args.eup[0], args.estep[0],
            xshift=args.xshift,
        )
        dictid2 = dat.merge_edge_spectra(
            args.edge, args.thickness, args.anglex, args.angley, pp=True,
        )
        fig, ax = dat.plot_spectra_pp(
            args.edge, args.thickness, args.anglex, args.angley,
            fig=fig, ax=ax, labelprefix=labelp,
        )
        
        if not args.compare:
            fig, ax = apply_plotopt(
                fig, ax, ylim=args.ylim, out=args.out, log=args.logscale,
                quiet=args.quiet, suppressout=args.suppressout, ncol=args.ncol[0],
                legendpos=args.legendpos[0],
            )
            fig, ax = plt.subplots()
        
        # Overwrite object
        if args.readwrite:
            dat.save_obj(filename)
    
    
    if args.compare:
        fig, ax = apply_plotopt(
            fig, ax, ylim=args.ylim, out=args.out, log=args.logscale,
            quiet=args.quiet, suppressout=args.suppressout, ncol=args.ncol[0],
            legendpos=args.legendpos[0],
        )
        fig.savefig(args.out)


def parse_cmd_args(argv):
    """Parse command line arguments using argparse
    """
    
    # Set up command line arg parser
    if argv is None:
        argv = sys.argv
    parser = argparse.ArgumentParser(
        prog=argv[0],
        description='Program to plot pickled dyndiff-mdff simulation data',
    )
    parser.add_argument(
        '-c', '--compare', action='store_true',
        help='command line switch to trigger comparison mode, i.e. data' \
        'from datafiles is compared in plots and no separate plots are produced',
    )
    parser.add_argument(
        '-dp', '--droppoints', default=None, type=int, nargs='+',
        help='Enter list of spectrum indices to drop before analysis.',
    )
    parser.add_argument(
        '-i', '--interactive', action='store_true',
        help='command line switch to enter interactive mode',
    )
    parser.add_argument(
        '-n', '--ncol', default=[2], type=int, nargs=1,
    )
    parser.add_argument(
        '--legendpos', default=['h'], type=str, nargs=1,
    )
    parser.add_argument(
        '-q', '--quiet', action='store_true',
        help='quiet mode. No plots are shown.',
    )
    parser.add_argument(
        '-rw', '--readwrite', action='store_true',
        help='Read/Write mode. Allows to modify datafile(s).',
    )
    parser.add_argument(
        '-so', '--suppressout', action='store_true',
        help='Suppress output to file.',
    )
    parser.add_argument(
        '-v', '--version', action='version', version='%(prog)s 0.1',
    )
    
    
    # Add subparsers to select type of plot
    subparsers = parser.add_subparsers(
        title='Plot mode',
        help='Select wether to plot diffraction pattern or spectra',
    )
    
    
    # Parser for raw DSCS spectra plotting
    parser_raw = subparsers.add_parser(
        'raw', help='specify that raw energy spectrum is to be plotted'
    )
    parser_raw.add_argument(
        '--anglex', nargs='+', default=6., type=float,
        help='specify x component of scattering angle (thetax) for spectrum plots',
    )
    parser_raw.add_argument(
        '--angley', nargs='+', default=6., type=float,
        help='specify y-component of scattering angle (thetay) for spectrum plots',
    )
    parser_raw.add_argument(
        '--datalabel', nargs='+', default=[''],
        help='command line switch to trigger log scale in plots',
    )
    parser_raw.add_argument(
        '--edge', default=0, nargs='+', 
        help='Specify edge(s) for plot', type=int,
    )
    parser_raw.add_argument(
        '-l', '--logscale', action='store_true',
        help='command line switch to trigger log scale in plots',
    )
    parser_raw.add_argument(
        '--thickness', nargs='+', default=['20.0'],
        help='specify thickness for plot(s)',
    )
    parser_raw.add_argument(
        '-o', '--out', default='dyndiffmdff_specdefault.png', 
        help='output file', type=str,
    )
    parser_raw.add_argument(
        '--xshift', nargs='+', default=[708., 721.],
        help='specify thickness for plot(s)', type=float,
    )
    parser_raw.add_argument(
        '-x', '--xlim', nargs=2, default='auto',
        help='limits for x-axis', type=str,
    )
    parser_raw.add_argument(
        '-y', '--ylim', nargs=2, default='auto',
        help='limits for y-axis', type=str,
    )
    
    # Add subparser to distinguish type of postprocessing
    subparsers_raw = parser_raw.add_subparsers(
        title='Raw data plotting',
        help='Select wether to plot diffraction pattern or spectra',
    )
    
    # Parser for raw spectra mode
    parser_raw_spec = subparsers_raw.add_parser(
        'spec', help='raw dyndifmdff data plotting'
    )
    parser_raw_spec.add_argument(
        'datafile', nargs='+', 
        help='file to be read',
    )
    parser_raw.add_argument(
        '--edgelabel', nargs='+', default=None, type=str,
        help='Label for edge(s)',
    )
    parser_raw.set_defaults(func=rawspec)
    
    
    # Parser for diffraction pattern plotting
    parser_raw_diffpatt = subparsers_raw.add_parser(
        'diffpatt', help='specify that diffraction pattern is to be plotted'
    )
    parser_raw_diffpatt.add_argument(
        '-c', '--clim', nargs=2, default='auto',
        help='value limits for color bar', type=str,
    )
    parser_raw_diffpatt.add_argument(
        'datafile', nargs='+', 
        help='file to be read',
    )
    parser_raw_diffpatt.add_argument(
        '--edge', default=['0'], nargs='+', 
        help='Specify edge(s) for plot',
    )
    parser_raw_diffpatt.add_argument(
        '-e', '--energy', nargs=1, default='int',
        help='specify energy of interest',
    )
    parser_raw_diffpatt.add_argument(
        '--energyrange', nargs=2, default='all',
        help='specify energy integration range for diffraction pattern',
    )
    parser_raw_diffpatt.add_argument(
        '-o', '--out', default='dyndifmdff_raw_diffpatt.png', 
        help='output file', type=str,
    )
    parser_raw_diffpatt.set_defaults(func=rawdiffpatt)
    
    
    # Parser for plotting of postprocessed spectra
    parser_pp = subparsers.add_parser(
        'pp', help='specify that energy spectrum is to be plotted'
    )
    parser_pp.set_defaults(func=ppspec)
    parser_pp.add_argument(
        '--anglex', nargs='+', default=6., type=float,
        help='specify x component of scattering angle (thetax) for spectrum plots',
    )
    parser_pp.add_argument(
        '--angley', nargs='+', default=6., type=float,
        help='specify y-component of scattering angle (thetay) for spectrum plots',
    )
    parser_pp.add_argument(
        '--datalabel', nargs='+', default=[''],
        help='command line switch to trigger log scale in plots',
    )
    parser_pp.add_argument(
        '--elow', nargs=1, default=[700.],
        help='specify lower bound for postprocessed spectrum',
        type=float,
    )
    parser_pp.add_argument(
        '--eup', nargs=1, default=[730.],
        help='specify upper bound for postprocessed spectrum',
        type=float,
    )
    parser_pp.add_argument(
        '--estep', nargs=1, default=[.05],
        help='specify energy step width for postprocessed spectrum',
        type=float,
    )
    parser_pp.add_argument(
        '--edge', default=[0, 1], nargs='+', 
        help='Specify edge(s) for plot', type=int,
    )
    parser_pp.add_argument(
        '--gamma', default=[.7, 1.2], nargs='+', 
        help='Specify edge(s) for plot',
    )
    parser_pp.add_argument(
        '-l', '--logscale', action='store_true',
        help='command line switch to trigger log scale in plots',
    )
    parser_pp.add_argument(
        '--thickness', nargs='+', default=['20.0'],
        help='specify thickness for plot(s)',
    )
    parser_pp.add_argument(
        '-o', '--out', default='dyndiffmdff_ppdefault.png', 
        help='output file', type=str,
    )
    parser_pp.add_argument(
        '--xshift', nargs='+', default=[708., 721.],
        help='specify thickness for plot(s)', type=float,
    )
    parser_pp.add_argument(
        '-y', '--ylim', nargs=2, default='auto',
        help='limits for y-axis', type=str,
    )
    
    
    # Add subparser for emcd calculation and plotting
    subparsers_pp = parser_pp.add_subparsers(
        title='Post Processing',
        help='Select wether to plot diffraction pattern or spectra',
    )
    
    
    # Parser for emcd spectra plotting
    parser_pp_spec = subparsers_pp.add_parser(
        'spec', help='specify that broadened and shifted spectrum is to be plotted'
    )
    parser_pp_spec.set_defaults(func=ppspec)
    parser_pp_spec.add_argument(
        'datafile', nargs='+', help='file to be read',
    )
    
    
    # Parser for emcd spectra plotting
    parser_pp_emcd = subparsers_pp.add_parser(
        'emcd', help='specify that emcd signal is to be considered'
    )
    parser_pp_emcd.set_defaults(func=emcd)
    parser_pp_emcd.add_argument(
        '-ds', '--detectorshape', nargs=1, type=str, default=['e'],
        help='Shape of detector. Supported is only q ("quadratic") for now',
    )
    parser_pp_emcd.add_argument(
        '-dx', '--detectorsizex', nargs='+', type=float,
        help='file to be read',
    )
    parser_pp_emcd.add_argument(
        '-dy', '--detectorsizey', nargs='+', type=float,
        help='file to be read',
    )
    parser_pp_emcd.add_argument(
        '-ntd', '--normtodetarea', action='store_true',
        help='Only EMCD signal is plotted',
    )
    parser_pp_emcd.add_argument(
        '--npe', action='store_true',
        help='Disable postedge normalization',
    )
    parser_pp_emcd.add_argument(
        '--penormdatatheo', nargs='+', type=str, default=None,
        help='file(s) containing aggregated spectra for post edge normalization' \
            'of the first theoretical emcd data file(s).',
    )
    parser_pp_emcd.add_argument(
        '--penormdataexp', nargs='+', type=str, default=None,
        help='file(s) containing aggregated spectra for post edge normalization' \
            'of the first experimental emcd data file(s).',
    )
    parser_pp_emcd.add_argument(
        '--pelow', nargs='+', default=[745.0], type=float,
        help='lower boundary for post edge normalization',
    )
    parser_pp_emcd.add_argument(
        '--peup', nargs='+', default=[760.0], type=float,
        help='upper boundary for post edge normalization',
    )
    parser_pp_emcd.add_argument(
        '-t', '--emcdtype', nargs='+', type=str, default=['vexp'],
        help='file to be read',
    )
    parser_pp_emcd.add_argument(
        '--sep', action='store_true',
        help='separator keyword',
    )
    
    
    # Add subparser for emcd calculation and plotting
    subparsers_pp_emcd = parser_pp_emcd.add_subparsers(
        title='',
        help='choose whether to plot emcd spectra or ',
    )
    
    
    # Parser for emcd spectra plotting
    parser_pp_emcd_spec = subparsers_pp_emcd.add_parser(
        'spec', help='specify that emcd ml/ms map is to be plotted'
    )
    parser_pp_emcd_spec.set_defaults(func=emcd)
    parser_pp_emcd_spec.add_argument(
        'datafile', nargs='+', help='file(s) to be read',
    )
    parser_pp_emcd_spec.add_argument(
        '-eo', '--emcdoff', action='store_true',
        help='EMCD signal is not plotted',
    )
    parser_pp_emcd_spec.add_argument(
        '-co', '--cemcdoff', action='store_true',
        help='Cumulative EMCD (cEMCD) is not plotted',
    )
    parser_pp_emcd_spec.add_argument(
        '-ps', '--plotspectra', action='store_true',
        help='Spectra from which EMCD is extracted are plotted',
    )
    
    
    # Parser for emcd ml/ms-map plotting
    parser_pp_emcd_mlmsmap = subparsers_pp_emcd.add_parser(
        'mlmsmap', help='specify that emcd ml/ms map is to be plotted'
    )
    parser_pp_emcd_mlmsmap.set_defaults(func=mlmsmap)
    parser_pp_emcd_mlmsmap.add_argument(
        '-c', '--clim', nargs=2, default='auto',
        help='value limits for color bar', type=str,
    )
    parser_pp_emcd_mlmsmap.add_argument(
        'datafile', nargs='+', help='file(s) to be read',
    )
    parser_pp_emcd_mlmsmap.add_argument(
        '--detcxminmax', nargs=2,default=[0.0, 25.0], type=float,
        help='min and max x-detector position for map',
    )
    parser_pp_emcd_mlmsmap.add_argument(
        '--detcyminmax', nargs=2, default=[0.0, 25.0], type=float,
        help='min and max y-detector position for map',
    )
    parser_pp_emcd_mlmsmap.add_argument(
        '--mlmsint', nargs=2, type=float,
        help='interval for ml/ms evaluation in case no'\
            'postedge normalisation is performed',
    )
    parser_pp_emcd_mlmsmap.add_argument(
        '--mlmsintnpe', nargs=2, type=float,
        help='interval for ml/ms evaluation in case no'\
            'postedge normalisation is performed',
    )
    
    
    # Parser for diffraction pattern plotting
    parser_diffpatt = subparsers.add_parser(
        'diffpatt', help='specify that diffraction pattern is to be plotted'
    )
    parser_diffpatt.set_defaults(func=diffpatt)
    parser_diffpatt.add_argument(
        '-c', '--clim', nargs=2, default='auto',
        help='value limits for color bar', type=str,
    )
    parser_diffpatt.add_argument(
        '-e', '--energy', nargs=1, default='int',
        help='specify energy of interest',
    )
    parser_diffpatt.add_argument(
        '--edge', default=[0, 1], nargs='+', 
        help='Specify edge(s) for plot',
    )
    parser_diffpatt.add_argument(
        '-l', '--logscale', action='store_true',
        help='command line switch to trigger log scale in plots',
    )
    parser_diffpatt.add_argument(
        '-o', '--out', default='dyndifmdff_diffpatt.png', 
        help='output file', type=str,
    )
    parser_diffpatt.add_argument(
        '--energyrange', nargs=2, default='all',
        help='specify energy integration range for diffraction pattern',
    )
    parser_diffpatt.add_argument(
        '--sign', action='store_true',
        help='difference mode.',
    )
    parser_diffpatt.add_argument(
        '--thickness', nargs='+', default=['20.0'],
        help='specify thickness for plot(s)',
    )
    parser_diffpatt.add_argument(
        '--sep', action='store_true',
        help='separator keyword',
    )


    subparsers_diffpatt = parser_diffpatt.add_subparsers(
        title='diffraction pattern plotting',
        help='Select wether to plot single diffraction pattern or add them',
    )
   
    # Plot one by one
    parser_diffpatt_plot1b1 = subparsers_diffpatt.add_parser(
        'plot1b1', help='add diffpatts'
    )
    parser_diffpatt_plot1b1.set_defaults(func=adddiffpatts)
    parser_diffpatt_plot1b1.add_argument(
        'datafile', nargs='+', 
        help='file to be read',
    )
    
    
    # Adding diffpatts
    parser_diffpatt_add = subparsers_diffpatt.add_parser(
        'add', help='add diffpatts'
    )
    parser_diffpatt_add.set_defaults(func=adddiffpatts)
    parser_diffpatt_add.add_argument(
        'datafile', nargs='+', 
        help='file to be read',
    )
    parser_diffpatt_add.add_argument(
        '--addedges', action='store_true',
        help='Add edges pixel-by-pixel?',
    )
    parser_diffpatt_add.add_argument(
        '--sign', nargs='+', type=str, default=[],
        help='sign for addition',
    )
    parser_diffpatt_add.add_argument(
        '--mirror', nargs='+', type=str, default=[],
        help='mirror diffpatt? n/h/v',
    )
    parser_diffpatt_add.add_argument(
        '--normpbp', action='store_true',
        help='Switch specifying pixel-by-pixel normalization.',
    )
    parser_diffpatt_add.add_argument(
        '--normcontrib', nargs='+', type=str, default=[],
        help='Specify contribution to normalization',
    )
    
    
    # Get and return command line options, skip program name in argv[0]
    argv=argv[1:]
    return parser.parse_args(argv)
    
    
    
    

def main(argv=None):
    """ Wrapper function representing the actual program.
    """
    
    # Parse command line input
    args = parse_cmd_args(argv)
   
    # Check that enoguh datafiles are specified for compare mode
    if args.compare:
        assert len(args.datafile) > 1, 'option -c (--compare) specified but \
            only one datafile given'

    # Invoke associated functions
    args.func(args)
    


# run main if not imported
if __name__ == '__main__':
    sys.exit(main(sys.argv))


